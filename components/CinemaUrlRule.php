<?php

namespace app\components;

use yii\web\UrlRule;
use yii\db\Command;
use yii\web\NotFoundHttpException;

class CinemaUrlRule extends UrlRule
{
    const DEFAULT_HALL=1;
    public $pattern = 'cinema';
    public $route = 'cinema';
    
    
    public function createUrl($manager, $route, $params)
    {

        if ($route === 'cinema/view') {
            if (isset($params['id'])) {
                $connection = \Yii::$app->db;
                $seo_name = $connection->createCommand("SELECT seo_name FROM cinema where id =".$params['id'])
                        ->queryScalar();
                if(isset($_GET['hall']))$hall=$_GET['hall'];
                else $hall=self::DEFAULT_HALL;
                return "cinema/$seo_name?hall=".$hall;

            } 
        }
        
        if ($route === 'film/view') {
            if (isset($params['id'])) {
                $connection = \Yii::$app->db;
                $seo_name = $connection->createCommand("SELECT seo_name FROM film where id =".$params['id'])
                        ->queryScalar();
                return "film/$seo_name";

            } 
        }
        
        if ($route === 'schedule/view') {
            if (isset($params['id'])) {
                $id=$params['id'];
                return "schedule/$id/places";

            } 
        }
        return false;  // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        
        $pathInfo = $request->getPathInfo();
        $route=explode("/", $pathInfo);


        
        if ($route[0]=='cinema') {
            if(isset($route[1])){
            if ($route[1]=='view') {
                throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
            }
            $seo_url=$route[1];
            $connection = \Yii::$app->db;
                $id = $connection->createCommand("SELECT id FROM cinema where seo_name='$seo_url'")
                        ->queryScalar();
                if(isset($_GET['hall']))$hall=$_GET['hall'];
                else $hall=self::DEFAULT_HALL;
                if($id!=Null){
                    return ['cinema/view', ['id'=>$id,'hall'=>$hall]];
                }

        }
        
                }
       
       if ($route[0]=='film') {
           if(isset($route[1])){
            if ($route[1]=='view') {
                throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
            }
            
            $seo_url=$route[1];
            $connection = \Yii::$app->db;
                $id = $connection->createCommand("SELECT id FROM film where seo_name='$seo_url'")
                        ->queryScalar();
                if($id!=Null){
                    return ['film/view', ['id'=>$id]];
                }

       }       }

       
       if ($route[0]=='schedule') {
           if(isset($route[1],$route[2])&&$route[2]=='places'){
               return ['schedule/view', ['id'=>$route[1]]];

       
       }
                }
        
        return false;  // this rule does not apply
    }
}