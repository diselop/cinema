<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cinema;

/**
 * CinemaSearch represents the model behind the search form about `app\models\Cinema`.
 */
class CinemaSearch extends Cinema
{
    public function rules()
    {
        return [
            [['id', 'name', 'seo_name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Cinema::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'seo_name', $this->seo_name]);

        return $dataProvider;
    }
}
