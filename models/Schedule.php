<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schedule".
 *
 * @property integer $id
 * @property string $date
 * @property string $time_start
 * @property string $time_end
 * @property integer $film
 * @property integer $hall
 *
 * @property Hall $hall0
 * @property Film $film0
 * @property Tickets[] $tickets
 */
class Schedule extends \yii\db\ActiveRecord
{
    public $tickets;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['time_start', 'time_end'], 'string'],
            [['film', 'hall'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'film' => 'Film',
            'hall' => 'Hall',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHall0()
    {
        return $this->hasOne(Hall::className(), ['id' => 'hall']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm0()
    {
        return $this->hasOne(Film::className(), ['id' => 'film']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Tickets::className(), ['schedule' => 'id']);
    }
}
