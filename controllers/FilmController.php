<?php

namespace app\controllers;

use Yii;
use app\models\Film;
use app\models\search\FilmSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use  yii\helpers\Json;
use app\components\JsonTranslate;

/**
 * FilmController implements the CRUD actions for Film model.
 */
class FilmController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Film models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FilmSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Film model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $db=  Yii::$app->db;
        $films = $db->createCommand('select name, number_hall,date, time_start, time_end from schedule s 
            inner join hall h on h.id=s.hall 
            inner join cinema c on c.id=h.cinema 
            where s.film=:id 
            order by date,time_start,name,number_hall
            ', [':id' => $id])->queryAll();
        echo JsonTranslate::normJsonStr(json_encode($films));
        /*
        $dataProvider = new SqlDataProvider([
            'sql' => "select name, number_hall,date, time_start, time_end from schedule s 
        inner join hall h on h.id=s.hall 
        inner join cinema c on c.id=h.cinema 
        where s.film=:id 
        order by date,time_start,name,number_hall",
            'params' => [':id' => $id],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
           */
                /*return $this->render('view', [
                    'model' => $this->findModel($id),
                    'dataProvider'=>$dataProvider,
                ]);*/
    }

    /**
     * Creates a new Film model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Film;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Film model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Film model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Film model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Film the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Film::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
