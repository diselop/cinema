<?php

namespace app\controllers;

use Yii;
use app\models\Cinema;
use app\models\search\CinemaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\components\JsonTranslate;
/**
 * CinemaController implements the CRUD actions for Cinema model.
 */
class CinemaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cinema models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CinemaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Cinema model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $db=  Yii::$app->db;
        $hallId = $db->createCommand('SELECT id FROM hall  WHERE number_hall=:number and cinema=:cinema
            ', [':number'=>$_GET['hall'],':cinema'=>$_GET['id']])->queryScalar();
        $cinema= $db->createCommand("select date, time_start, time_end, name "
            . "from schedule s inner join film f on f.id=s.film "
            . "where hall=:hallId order by date,time_start"
            , [':hallId' => $hallId])->queryAll();
        echo JsonTranslate::normJsonStr(json_encode($cinema));
    }
 // for gui       
/*$hallId = $db->createCommand('
    SELECT id FROM hall  WHERE number_hall=:number and cinema=:cinema
', [':number'=>$_GET['hall'],':cinema'=>$_GET['id']])->queryScalar();
$count = $db->createCommand('
    SELECT COUNT(id) FROM schedule  WHERE hall=:hall
', [':hall'=>$hallId])->queryScalar();

$dataProvider = new SqlDataProvider([
    'sql' => "select date, time_start, time_end, name "
        . "from schedule s inner join film f on f.id=s.film "
        . "where hall=:hallId order by date,time_start",
    'params' => [':hallId' => $hallId],
    'totalCount' => $count,
    'pagination' => [
        'pageSize' => 20,
    ],
]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider'=>$dataProvider,
        ]);
    }*/

    /**
     * Creates a new Cinema model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cinema;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Cinema model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Cinema model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cinema model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Cinema the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cinema::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
