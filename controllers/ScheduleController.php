<?php

namespace app\controllers;

use Yii;
use app\models\Schedule;
use app\models\search\ScheduleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\JsonTranslate;

/**
 * ScheduleController implements the CRUD actions for Schedule model.
 */
class ScheduleController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'buy' => ['post'],
                    'reject' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Schedule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScheduleSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Schedule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $db=  Yii::$app->db;
        $schedule= $db->createCommand("select id from tickets where schedule=:id limit 1"
            , [':id' => $id])->queryScalar();
        if($schedule!=FALSE){
            $places= $db->createCommand("select unnest(free(array_merge(places),100))as place from tickets  where schedule=:id"
                , [':id' => $id])->queryColumn();
            $result=$places;
        }
        else   $result=['message'=>'Данный сеанс отсутствует'];
        echo JsonTranslate::normJsonStr(json_encode($result));
        /*$count = $db->createCommand('
            select array_upper(free(places,100),1) from tickets where id=:id
        ', [':id' => $id])->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => "select unnest(free(array_merge(places),100))as place from tickets
             where schedule=:id",
            'params' => [':id' => $id],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => $count,
            ],
        ]);
                return $this->render('view', [
                    'model' => $this->findModel($id),
                    'dataProvider'=>$dataProvider,
                ]);*/
    }
    
    public function actionBuy($session){
        $db=  Yii::$app->db;
        $places='{'.Yii::$app->request->getQueryParam('places').'}';
        $busy = $db->createCommand('select :places && array_merge(places) from tickets  where schedule=:session
            ', [':places'=>$places,':session'=>$session])->queryScalar();
        $outPlace=$db->createCommand('select out_range(:places,150)
            ', [':places'=>$places])->queryScalar();
        if($busy==true){
            $result=['message'=>'Некоторые места уже заняты'];
        }
        elseif($outPlace==true){
            $result=['message'=>'Заявка на несуществующие места']; 
        }   
        else {
            $id = $db->createCommand('select insert_ticket(:places,:session)
            ', [':places'=>$places,':session'=>$session])->queryScalar();
            $result=['message'=>'Места успешно куплены','id'=>$id];
        }
        echo JsonTranslate::normJsonStr(json_encode($result));
        
    }
    
public function actionReject($id){
    $db=  Yii::$app->db;
    $schedule=$db->createCommand('SELECT schedule  FROM tickets where id=:id 
        ', [':id'=>$id])->queryScalar();
    $hour = $db->createCommand('select (EXTRACT(epoch FROM (date+time_start)-now())/3600) from schedule where id=:id
    ', [':id'=>$schedule])->queryScalar();
    if($hour==null)$result=['message'=>'Билет не найден']; 
    elseif($hour<1)$result=['message'=>'Вы опоздали, нужно сдать билет не ранее чем за час']; 
    else {
        $db->createCommand('DELETE FROM tickets WHERE id=:id
            ', [':id'=>$id])->execute();
        $result=['message'=>'Покупка отменена'];
    }
    echo JsonTranslate::normJsonStr(json_encode($result));
}

    /**
     * Creates a new Schedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schedule;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Schedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Schedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schedule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
