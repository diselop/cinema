<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Cinema $model
 */

$this->title = 'Create Cinema';
$this->params['breadcrumbs'][] = ['label' => 'Cinemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cinema-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
