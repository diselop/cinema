<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\CinemaSearch $searchModel
 */

$this->title = 'Кинотеатры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cinema-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'seo_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
