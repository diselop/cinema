<?php

use yii\helpers\Html;
use yii\grid\GridView;
/**
 * @var yii\web\View $this
 * @var app\models\Film $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Фильмы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="film-view">

    <h1><?= Html::encode($this->title) ?></h1>

 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name:text:Кинотеатр',
            'number_hall:integer:Зал',
            'date:date:Дата',
            'time_start:time:Начало',
            'time_end:time:Окончание',
            
            

           
        ],
    ]);
?>
</div>
