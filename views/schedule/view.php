<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var app\models\Schedule $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Сеансы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="schedule-view">
    <h1>Свободные места</h1>
<p>

        <?= Html::a('Забранировать', ['buy', 'session' => $model->id,'places'=>'1,2,3'], [
            'class' => 'btn btn-success',
            'data' => [
                'method' => 'post',
            ],
        ]) ?>
    </p>
 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'place:integer:Место',  
            ]

    ]);
?>

</div>
